<?php
    include_once 'llamadas.php';

    class ApiLlamadas{
        function getAll(){
            $Llamada = new Llamada();
            $llamadas = array();
            $llamadas["Llamada"] = array();
            $res = $Llamada->obtenerLlamadas();
            if($res->rowCount()){
                while ($row = $res->fetch(PDO::FETCH_ASSOC)){

                    $link = "https://172.16.96.253/grabaciones/";
                    $audioPath = $row['recordingfile'];
                    $audioLink = $link . $audioPath;

                    $item=array(
                        "uniqueid" => $row['uniqueid'],
                        "audio" => $audioLink,
                    );
                    array_push($llamadas["Llamada"], $item);
                }
            
                $this->printJSON($llamadas);
            }else{
                echo json_encode(array('mensaje' => 'No hay elementos'));
            }
        }

        function getById($uniqueid){
            $llamada = new Llamada();
            $llamadas = array();
            $llamadas["Llamada"] = array();

            $res = $llamada->obtenerLlamada($uniqueid);

            if($res->rowCount() == 1){
                $row = $res->fetch();
                
                $link = "https://172.16.96.253/grabaciones/";
                $audioPath = $row['recordingfile'];
                $audioLink = $link . $audioPath;

                $item=array(
                    "uniqueid" => $row['uniqueid'],
                    "audio" => $audioLink,
                );
                array_push($llamadas["Llamada"], $item);
                $this->printJSON($llamadas);
            }else{
                
                    echo json_encode(array('mensaje' => 'No hay elementos'));
                }

        function error($mensaje){
            echo json_encode(array('mensaje' => $mensaje)); 
        }
        function printJSON($array){
            echo '<code>'.json_encode($array).'</code>';
        }
    }
?>