<?php

    class DB{
        private $host;
        private $db;
        private $user;
        private $password;
        private $charset;
        public function __construct(){
            $this->host     = 'localhost';
            $this->db       = 'call_center_msi';
            $this->user     = 'u_consultas';
            $this->password = "f8093689e2520fdbbe415ef229ecb723";
            $this->charset  = 'utf8mb4';
        }

        function connect(){
        
            try{
                
                $connection = "mysql:host=".$this->host.";dbname=" . $this->db . ";charset=" . $this->charset;
                $options = [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES   => false,
                ];
                $pdo = new PDO($connection,$this->user,$this->password);
            
                return $pdo;
            }catch(PDOException $e){
                print_r('Error connection: ' . $e->getMessage());
            }   
        }
    }

    class DB_old{
        private $host;
        private $db;
        private $user;
        private $password;
        private $charset;
        public function __construct(){
            $this->host     = '172.16.96.2';
            $this->db       = 'asteriskcdrdb';
            $this->user     = 'u_consultas';
            $this->password = "f8093689e2520fdbbe415ef229ecb723";
            $this->charset  = 'utf8mb4';
        }

        function connect(){
        
            try{
                
                $connection = "mysql:host=".$this->host.";dbname=" . $this->db . ";charset=" . $this->charset;
                $options = [
                    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_EMULATE_PREPARES   => false,
                ];
                $pdo = new PDO($connection,$this->user,$this->password);
            
                return $pdo;
            }catch(PDOException $e){
                print_r('Error connection: ' . $e->getMessage());
            }   
        }
    }

?>